#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    // appel des fontion d'alimentation des listes
    FlightLoader(":/CSV/flightCsv");
    AirportLoader(":/CSV/airportCsv");


    // appel des fonctions qui alimente les listes en UI
    BtnListMaker("flight");
    BtnListMaker("airport");

}


Dialog::~Dialog() // destructeur
{
    delete ui;
}

void Dialog::FlightLoader(QString _urlStr){ // alimentation de la liste des vols
    FL.flightLoading(_urlStr);
    for(int hint = 0;hint < FL.getVecSize("Flight");hint++){
        this->flightList.push_back(FL.getFlightInVec(hint));
    }
}
void Dialog::AirportLoader(QString _urlStr){ // alimentation de la liste des aeroports

    FL.airportLoading(_urlStr);
    for(int hint = 0;hint < FL.getVecSize("Airport");hint++){
        this->airportList.push_back(FL.getAirportInVec(hint));
    }
}

void Dialog::BtnListMaker(QString _object){ // fonction d'alimentation d'elements des listes
    if(_object.toLower() == "flight"){ // verifie si la liste demander est flight
        for(int hint = 0;hint < flightList.size();hint++){ // il boucle tant que in n'ateint pas la taille de la liste
            QListWidgetItem* lItem = new QListWidgetItem; // definie un item
            lItem->setSizeHint(QSize(100,50)); // definie la taille de l'item
            QVariant id; // creation d'un parametre a passer a l'item
            id.setValue(hint); // definition de la valeur du parametre suivant l'indice de la liste
            lItem->setData(1,hint); // ajout du parametre a l'item
            lItem->setText(flightList[hint]->getName()); // definition du nom afficher de l'item
            ui->FLightListBtnList->addItem(lItem); // ajout de l'item a la liste
        }
    }else{// verifie sinon si la liste demander est aeroport
        for(int hint = 0;hint < airportList.size();hint++){
            QListWidgetItem* lItem = new QListWidgetItem;
            lItem->setSizeHint(QSize(100,50));
            QVariant id;
            id.setValue(hint);
            lItem->setData(1,hint);
            lItem->setText(airportList[hint]->getName());
            ui->AirportListBtnList->addItem(lItem);
        }
    }
}

//Réinitilisation des points sur la carte et des noms de la liste (enlève le fond rouge)
void Dialog::InitStateAirportButton(){
    ui->pushButton_Aeroport1->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport2->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport3->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport4->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport5->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport6->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport7->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport8->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport9->setStyleSheet("background-color: white;");
}


//Les fonctions en cliquant sur les bouton de la map

void Dialog::on_pushButton_Aeroport1_clicked()
{
    if(!airportList.isEmpty()){
        QString longi = QString::number(airportList[0]->getLongitude());
        QString lati = QString::number(airportList[0]->getLatitude());
        QString alti = QString::number(airportList[0]->getAltitude());
        QString lb = longi + "," + lati + "," + alti;
        ui->label_Coordonnees_1->setText(lb);
        airportList[0]->setModal(true);
        airportList[0]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
    ui->label_Coordonnees_1->setText("");
}

void Dialog::on_pushButton_Aeroport2_clicked()
{
    if(!airportList.isEmpty()){
        airportList[1]->setModal(true);
        airportList[1]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
}

void Dialog::on_pushButton_Aeroport3_clicked()
{

    if(!airportList.isEmpty()){
        airportList[2]->setModal(true);
        airportList[2]->exec();
    }else{
        qDebug() << "flightList is empty";
    }

}


void Dialog::on_pushButton_Aeroport4_clicked()
{
    if(!airportList.isEmpty()){
        airportList[3]->setModal(true);
        airportList[3]->exec();
    }else{
        qDebug() << "flightList is empty";
    }

}

void Dialog::on_pushButton_Aeroport5_clicked()
{
    if(!airportList.isEmpty()){
        airportList[4]->setModal(true);
        airportList[4]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
}

void Dialog::on_pushButton_Aeroport6_clicked()
{
    if(!airportList.isEmpty()){
        airportList[5]->setModal(true);
        airportList[5]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
}


void Dialog::on_pushButton_Aeroport7_clicked()
{
    if(!airportList.isEmpty()){
        airportList[6]->setModal(true);
        airportList[6]->exec();
    }else{
        qDebug() << "flightList is empty";
    }

}


void Dialog::on_pushButton_Aeroport8_clicked()
{
    if(!airportList.isEmpty()){
        airportList[7]->setModal(true);
        airportList[7]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
}

void Dialog::on_pushButton_Aeroport9_clicked()
{
    if(!airportList.isEmpty()){
        airportList[8]->setModal(true);
        airportList[8]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
}



//Les fonctions en double cliquant sur les listes

void Dialog::on_FLightListBtnList_itemDoubleClicked(QListWidgetItem *item)
{

    if(!flightList.isEmpty()){ // verifie si la liste est vide
        int i = item->data(1).toInt(); // recupere l'identifiant de l'item de la liste
        flightList[i]->setModal(true); // ouvre une fenetre qui contiendra les donnée de l'objet de liste selection via 'i'
        flightList[i]->exec();
    }else{
        qDebug() << "flightList is empty";
    }
}


void Dialog::on_AirportListBtnList_itemDoubleClicked(QListWidgetItem *item)
{
    if(!airportList.isEmpty()){ // verifie si la liste est vide
        int i = item->data(1).toInt(); // recupere l'identifiant de l'item de la liste
        airportList[i]->setModal(true); // ouvre une fenetre qui contiendra les donnée de l'objet de liste selection via 'i'
        airportList[i]->exec();
    }else{
        qDebug() << "airportList is empty";
    }
}

