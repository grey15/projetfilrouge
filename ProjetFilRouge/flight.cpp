#include "flight.h"
#include "ui_flight.h"

Flight::Flight(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Flight)
{
    ui->setupUi(this);
}


Flight::~Flight()
{
    delete ui;
}



// les setter de la class


void Flight::setName(QString _name){
    ui->NameSpot->setText(_name);
    this->name = _name;
}


void Flight::addPosiHist(FlightData _posiHist){ // fonction d'ajout de position
    this->posiHist.push_back(_posiHist);

    QListWidgetItem* lItem = new QListWidgetItem();// creation d'un item pour la liste des position UI

    ui->listPosHist->addItem(lItem); // ajout de l'item a la liste UI
    lItem->setSizeHint(QSize(100,50)); // definition de la taille de l'item

    PosiListItem* posihistItem = new PosiListItem(); // creation d'un item pour la mise en forme des positions UI

    // definition de l'item
    posihistItem->setHeure(_posiHist.getHeure());
    posihistItem->setMinute(_posiHist.getMinute());
    posihistItem->setLongitude(_posiHist.getLongitude());
    posihistItem->setLatitude(_posiHist.getLatitude());
    posihistItem->setAltitude(_posiHist.getAltitude());
    // fin definition de l'item


    ui->listPosHist->setItemWidget(lItem, posihistItem); // definition de l'item avec la mise en forme

}

void Flight::fillPosiHist(QVector<FlightData> _vec){
    for (int var = 0; var < _vec.size(); ++var) {
        addPosiHist(_vec[var]);
    }
}




// les getter de la class


QVector<FlightData> Flight::getPosiHist(){
    return this->posiHist;
}

QString Flight::getName(){
    return this->name;
}
