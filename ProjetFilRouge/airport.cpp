#include "airport.h"
#include "ui_airport.h"


// le construteur de la class

Airport::Airport(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Airport)
{
    ui->setupUi(this);
}

// le destruteur de la class
Airport::~Airport()
{
    delete ui;
}

// les setter de la class
void Airport::setLongitude(double _longitude){
    ui->LongLB->setText(QString::number(_longitude));
    this->longitude = _longitude;
}
void Airport::setLatitude(double _latitude){
    ui->LatLB->setText(QString::number(_latitude));
    this->latitude = _latitude;
}
void Airport::setAltitude(double _altitude){
    ui->AltLB->setText(QString::number(_altitude));
    this->altitude = _altitude;
}
void Airport::setName(QString _name){
    ui->NameLB->setText(_name);
    this->name = _name;
}
void Airport::setNameEn(QString _nameEn){
    ui->NameEnLB->setText(_nameEn);
    this->nameEn = _nameEn;
}
void Airport::setNameFr(QString _nameFr){
    ui->NameFrLB->setText(_nameFr);
    this->nameFr = _nameFr;
}
void Airport::setIATACode(QString _iata){
    ui->IATAICodeLB->setText(_iata);
    this->IATACode = _iata;
}
void Airport::setICAOCode(QString _icao){
    ui->ICAOCodeLB->setText(_icao);
    this->ICAOCode = _icao;
}
void Airport::setWikidataId(QString _wikidataId){
    ui->WikidataIdLB->setText(_wikidataId);
    this->wikidataId = _wikidataId;
}
void Airport::setOperator(QString _sOperator){
    ui->OperatorLB->setText(_sOperator);
    this->sOperator = _sOperator;
}
void Airport::setCountry(QString _country){
    ui->CountryLB->setText(_country);
    this->country = _country;
}
void Airport::setCountryCode(QString _countryCode){
    ui->CountryCodeLB->setText(_countryCode);
    this->countryCode = _countryCode;
}



// les getter de la class

double Airport::getLongitude(){
    return this->longitude;
}
double Airport::getLatitude(){
    return this->latitude;
}
double Airport::getAltitude(){
    return this->altitude;
}
QString Airport::getName(){
    return this->name;
}
QString Airport::getNameEn(){
    return this->nameEn;
}
QString Airport::getNameFr(){
    return this->nameFr;
}
QString Airport::getIATACode(){
    return this->IATACode;
}
QString Airport::getICAOCode(){
    return this->ICAOCode;
}
QString Airport::getWikidataId(){
    return this->wikidataId;
}
QString Airport::getOperator(){
    return this->sOperator;
}
QString Airport::getCountry(){
    return this->country;
}
QString Airport::getCountryCode(){
    return this->countryCode;
}


