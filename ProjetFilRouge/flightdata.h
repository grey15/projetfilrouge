#ifndef FLIGHTDATA_H
#define FLIGHTDATA_H

#include <QObject>

class FlightData
{
public:
    FlightData();
    ~FlightData();

    void setHeure(int _heure);
    void setMinute(int _minute);
    void setLongitude(double _longitude);
    void setLatitude(double _latitude);
    void setAltitude(double _altitude);

    int getHeure();
    int getMinute();
    double getLongitude();
    double getLatitude();
    double getAltitude();

private:
    int heure;
    int minute;
    double longitude;
    double latitude;
    double altitude;
};

#endif // FLIGHTDATA_H
