#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QVector>
#include <QFile>
#include <QThread>

#include "flight.h"
#include "airport.h"
#include "flightdata.h"
#include "filesloader.h"


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

    void FlightLoader(QString);
    void AirportLoader(QString);
    void BtnListMaker(QString);

    void mapBtnFind();

    void InitStateAirportButton();

private slots:

    void on_pushButton_Aeroport1_clicked();

    void on_pushButton_Aeroport2_clicked();

    void on_pushButton_Aeroport3_clicked();

    void on_pushButton_Aeroport4_clicked();

    void on_pushButton_Aeroport5_clicked();

    void on_pushButton_Aeroport6_clicked();

    void on_pushButton_Aeroport7_clicked();

    void on_pushButton_Aeroport8_clicked();

    void on_pushButton_Aeroport9_clicked();


    void on_FLightListBtnList_itemDoubleClicked(QListWidgetItem *item);

    void on_AirportListBtnList_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::Dialog *ui;
//    Flight *flight1;
    QVector<Flight*> flightList;
    QVector<Airport*> airportList;
    Filesloader FL;
    FlightData flightData;
};

#endif // DIALOG_H
