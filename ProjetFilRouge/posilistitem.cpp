#include "posilistitem.h"
#include "ui_posilistitem.h"

PosiListItem::PosiListItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PosiListItem)
{
    ui->setupUi(this);
}

PosiListItem::~PosiListItem()
{
    delete ui;
}

// setter de la class
void PosiListItem::setHeure(int _heure){
    QString str = QString::number(_heure);
    ui->heureLB->setText(str);
    this->heure = _heure;
}
void PosiListItem::setMinute(int _minute){
    QString str = QString::number(_minute);
    ui->MinuteLB->setText(str);
    this->minute = _minute;
}
void PosiListItem::setLongitude(double _longitude){
    QString str = QString::number(_longitude);
    ui->LongLB->setText(str);
    this->longitude = _longitude;
}
void PosiListItem::setLatitude(double _latitude){
    QString str = QString::number(_latitude);
    ui->LatLB->setText(str);
    this->latitude = _latitude;
}
void PosiListItem::setAltitude(double _altitude){
    QString str = QString::number(_altitude);
    ui->AltLB->setText(str);
    this->altitude = _altitude;
}






