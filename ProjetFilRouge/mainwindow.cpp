#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "register.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_Quitter_clicked()
{
    QApplication::quit();
}


void MainWindow::on_pushButton_Log_in_clicked()
{

    QString ID = ui->lineEdit_ID->text();
    if (ID == ""){
        QMessageBox::information(this, "ID is Empty", "ID is Empty !");
        return;
    }

    QString PW = ui->lineEdit_Password->text();
    if (PW == ""){
        QMessageBox::information(this, "Password is Empty", "Password is Empty !");
        return;
    }



    if (ID=="1234" && PW=="1234"){ //Cas où l'ID et le mdp sont corrects
        this->hide(); //Ferme la page d'authentifiaction

        QMessageBox::information(this, "Success connection !", "Welcome on the application. Click on OK and enjoy the application");

        //Création d'un objet de type dialogue et ouverture de la deuxième fenêtre lorsque les identifiants sont bons
        Dialog dialog;
        dialog.setModal(true);
        dialog.exec();

    }
    else {
        cpt++;
        if(cpt<4){
        QMessageBox::warning(this, "Authentification failed", "ID and password combination does not exist !");
        } else {
        QMessageBox::critical(this, "Authentification failed", "Too numbers of attemps !");
        QApplication::quit();
        }
        return;
    }

}

void MainWindow::on_pushButton_Create_an_account_clicked()
{
    Register reg;
    reg.setModal(true);
    reg.exec();

}


