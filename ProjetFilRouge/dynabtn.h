#ifndef DYNABTN_H
#define DYNABTN_H

#include <QObject>
#include <QWidget>
#include <QPushButton>

template<typename t>

class DynaBtn : public QPushButton
{
public:
    explicit DynaBtn(QWidget *parent = 0);
    ~DynaBtn();

    void setText(QString);
    void setId(int);

public slots:


private:
    int id;
};

#endif // DYNABTN_H
