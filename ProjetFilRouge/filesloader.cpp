﻿#include "filesloader.h"


Filesloader::Filesloader(){

}


QString Filesloader::setFiles(QString _urlStr){ // fonction qui verifie que le document demander existe et puisse s'ouvrir

    QFile files =_urlStr;

    QString fileName = _urlStr.split('/').last(); // recupere le nom se trouvant a la fin de l'url

    if(files.exists()){ // si le ficher exist
        qDebug() << fileName << " is existe";
    }else{// si le ficher exist pas
        qDebug() << fileName << " is not existe";
    }

    if (files.open(QIODevice::ReadOnly)){// si le ficher s'ouvre
        qDebug() << fileName << "file is open";

    }

    return _urlStr; // il retourn le lien

}

void Filesloader::flightLoading(QString _urlStr){

    QFile files = setFiles(_urlStr);// recuperation du fichier csv
    FlightData flightData; // creation de l'objet de donnée de vol

    if (files.open(QIODevice::ReadOnly)){

        while (!files.atEnd()) {
            QString lineBrut = files.readLine(); // recuperation de la ligne a traiter
            QString line = lineBrut.split('\r').first(); // decoupe de la ligne afin de retiré le '\r' a la fin de ligne
            QStringList list =line.split(','); // segmentation de la ligne pour faciliter la repartition de données

            if(line != ""){ //ferification en cas de ligne vide

                // definition des donnée de vol
                flightData.setHeure(list.at(1).toInt());
                flightData.setMinute(list.at(2).toInt());
                flightData.setLongitude(list.at(3).toDouble());
                flightData.setLatitude(list.at(4).toDouble());
                flightData.setAltitude(list.at(5).toDouble());
                // fin definition des donnée de vol

                Flight *flight = new Flight(); // creation de l'objet de vol
                // definition de l'objet vol
                flight->setName(list.at(0));
                flight->addPosiHist(flightData);
                // fin definition de l'objet vol

                if(!flightLoad.isEmpty()){ // si la liste n'est pas vide

                    // verifie la presence du vol dans la liste flightLoad
                    // et retourne l'indice correspondant dans la list
                    int findHint = findOccurence(flight->getName(),flightLoad);

                    if(findON != false){ // si il a trouver le nom du vol dans la liste
                        flightLoad[findHint]->addPosiHist(flightData); // ajout de la position au vol trouver
                    }else{ // si il ne trouve pas
                        qDebug() << flight->getName() << " adding to list";
                        flightLoad.push_back(flight); // ajoute un nouveaux vol
                    }
                }
                else{ // si la liste est vide
                    qDebug() << flight->getName() << " is the first";
                    flightLoad.push_back(flight); // ajoute un nouveaux vol
                }
            }
        }

    }
    qDebug()<< "flight load finish";

}
void Filesloader::airportLoading(QString _urlStr){

    QFile files = setFiles(_urlStr); // recuperation du fichier csv


    if (files.open(QIODevice::ReadOnly)){

        while (!files.atEnd()) {
            QString lineBrut = files.readLine(); // recuperation de la ligne a traiter
            QString line = lineBrut.split('\r').first(); // decoupe de la ligne afin de retiré le '\r' a la fin de ligne
            line = line.split('\n').first(); // re-decoupe de la ligne afin de retiré le '\n' a la fin de ligne
            QStringList list =line.split(','); // segmentation de la ligne pour faciliter la repartition de données


            if(line != ""){

                Airport* airport1= new Airport(); // creation d'un objet pour l'incrementation

                // definition de l'objet
                airport1->setLongitude(list.at(0).toDouble());
                airport1->setLatitude(list.at(1).toDouble());
                airport1->setAltitude(list.at(11).toDouble());
                airport1->setName(list.at(2));
                airport1->setNameEn(list.at(3));
                airport1->setNameFr(list.at(4));
                airport1->setIATACode(list.at(5));
                airport1->setICAOCode(list.at(6));
                airport1->setWikidataId(list.at(7));
                airport1->setOperator(list.at(8));
                airport1->setCountry(list.at(9));
                airport1->setCountryCode(list.at(10));
                // fin de definition de l'objet

                this->airportLoad.push_back(airport1); // ajoute un nouveaux aeroport
                qDebug()<< airport1->getIATACode() << " is add to list";
            }
        }
    }

    qDebug()<< "airport load finish";

}


template<typename ty>
int Filesloader::findOccurence(QString _toFind,QVector<ty*> _list){
    int find = 0;
    for(int in = 0; in < _list.size(); in++){ // il boucle tant que in n'ateint pas la taille de la list passer en parametre
        if(_list[in]->getName() == _toFind){ // si le nom de l'objet de la liste a l'indice 'in' correspont a la valeur rechercher '_toFind'
            find = in; // find prend la valeur de 'in'
            this->findON = true; // change l'état de findOn
            break; // sort de la boucle
        }else{ // le nom correspond pas a la valeur trouver
            continue; // il continue de parcourir la boucle
        }
    }
    return find; // retourn la valeur de 'find'
}


// getter de la class

bool Filesloader::getFindON(){
    return this->findON; // recupere la valeur de 'findOn'
}

int Filesloader::getVecSize(QString _object){ // fonction qui renvoi la taille des liste suivant si '_objet' correcpond a flight ou autre
    QString objectName = _object.toLower();
    if(objectName == "flight"){
        return this->flightLoad.size();
    }else {
        return this->airportLoad.size();
    }
}


Flight* Filesloader::getFlightInVec(int _hint){ // renvoi l'objet de la list flightLoad a l'indice '_hint'
    return this->flightLoad[_hint];
}

Airport* Filesloader::getAirportInVec(int _hint){ // renvoi l'objet de la list airportLoad a l'indice '_hint'
    return this->airportLoad[_hint];
}


