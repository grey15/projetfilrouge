#ifndef FILESLOADER_H
#define FILESLOADER_H

#include <QObject>
#include <QVector>
#include <QDebug>
#include <QFile>

#include "flight.h"
#include "airport.h"



class Filesloader
{
public:
    Filesloader();
    QString setFiles(QString);

    void flightLoading(QString);
    void airportLoading(QString);

    Flight* getFlightInVec(int _hint);
    Airport* getAirportInVec(int _hint);



    template<typename ty>
    int findOccurence(QString,QVector<ty*>);

    bool getFindON();

    int getVecSize(QString);

private:
    QVector<Flight*> flightLoad;
    QVector<Airport*> airportLoad;
    bool findON = false;

};





#endif // FILESLOADER_H
