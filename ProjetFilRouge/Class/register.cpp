#include "Header/register.h"
#include "ui_register.h"


Register::Register(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Register)
{
    ui->setupUi(this);
}

Register::~Register()
{
    delete ui;
}

void Register::on_pushButton_Create_new_account_clicked()
{
    QString New_ID = ui->lineEdit_New_ID->text();
    if (New_ID == ""){
        QMessageBox::warning(this, "ID is Empty", "ID is Empty !");
        return; }

    QString New_Password = ui->lineEdit_New_Password->text();
    if (New_Password == ""){
        QMessageBox::warning(this, "Password is Empty", "Password is Empty !");
        return; }

    QString New_Password_2 = ui->lineEdit_New_Password_2->text();
    if (New_Password_2 == ""){
        QMessageBox::warning(this, "Confirm password is Empty", "Confirm password is Empty !");
        return; }

    if (New_Password != New_Password_2) {
        QMessageBox::warning(this, "Failed", "Passwords are not the same !");
        return; }

    if (New_Password == New_Password_2 && New_ID != ""){
        QMessageBox::information(this, "Success", "New account is created !");
        this->hide();
//        IDVector.push_back(New_ID);
//        PWVector.push_back(New_Password);
    }
}

//QVector<QString> Register::getIDVector(){
//    return IDVector;
//}

//QVector<QString> Register::getPWVector(){
//    return PWVector;
//}
