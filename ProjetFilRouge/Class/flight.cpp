#include "Header/flight.h"
#include "ui_flight.h"

Flight::Flight(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Flight)
{
    ui->setupUi(this);
}


Flight::~Flight()
{
    delete ui;
}

void Flight::display(){
    ui->NameSpot->setText(this->name);

//    for(FlightData in: this->posiHist){

//    }
}

void Flight::setName(QString _name){
    ui->NameSpot->setText(_name);
    this->name = _name;
}

QString Flight::getName(){
    return this->name;
}

void Flight::addPosiHist(FlightData _posiHist){
    this->posiHist.push_back(_posiHist);
}

QVector<FlightData> Flight::getPosiHist(){
    return this->posiHist;
}
