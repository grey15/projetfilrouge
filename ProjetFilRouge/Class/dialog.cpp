#include "Header/dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    ui->listWidget_Complete->hide();
    ui->label_Complete->hide();
    ui->label_Coordonnees_1->hide();
    ui->label_Coordonnees_2->hide();
    ui->label_Coordonnees_3->hide();
    ui->label_Coordonnees_4->hide();
    ui->label_Coordonnees_5->hide();
    ui->label_Coordonnees_6->hide();
    ui->label_Coordonnees_7->hide();
    ui->label_Coordonnees_8->hide();
    ui->label_Coordonnees_9->hide();

    QString url = ":/CSV/volCsv";
    QFile files(url);
        if(files.exists()){
            qDebug()<< "is existe";
        }else{
            qDebug()<< "is not existe";
        }

    if (files.open(QIODevice::ReadOnly)){
        qDebug() << "file is open";
    }

    while (!files.atEnd()) {
        QString lineBrut = files.readLine();
        QString line = lineBrut.split('\r').first();
        QStringList list =line.split(',');


        if(line != ""){
            //            qDebug()<< "line is :" << line << "\n";
            //            if(!name.isEmpty()){
            //                if(listingKey.isEmpty()){
            //                    listingKey.push_back(name);
            //                }else if (listingKey.contains(name)){
            //                    continue;
            //                }else{
            //                    listingKey.push_back(name);
            //                }
            //            }

            flightData.setHeure(list.at(1).toInt());
            flightData.setMinute(list.at(2).toInt());
            flightData.setLongitude(list.at(3).toDouble());
            flightData.setLatitude(list.at(4).toDouble());
            flightData.setAltitude(list.at(5).toDouble());

            Flight *flight = new Flight();
            flight->setName(list.at(0));
            flight->addPosiHist(flightData);

            int i = 0;
            if(flightList.isEmpty()){

                flightList.push_back(flight);
            }else{
                for(Flight in: flightList){
                    if(in.getName() == flight->getName()){
                        flightList.at(i)->addPosiHist(flightData);
                        i++;
                    }else{
                        flightList.push_back(flight);
                    }
                }
            }


        }
    }


}


Dialog::~Dialog()
{
    delete ui;
}

void Dialog::AlignItemsListAirports(){ //Alignement des Items des listes d'Airport

    ui->listWidget_Complete->item(0)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(1)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(2)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(3)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(4)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(5)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(6)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(7)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(8)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(9)->setTextAlignment(Qt::AlignCenter);
    ui->listWidget_Complete->item(10)->setTextAlignment(Qt::AlignCenter);

};

void Dialog::InitStateAirportButton(){ //Réinitilisation des points sur la carte et des noms de la liste (enlève le fond rouge)

    ui->pushButton_Aeroport1->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport2->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport3->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport4->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport5->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport6->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport7->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport8->setStyleSheet("background-color: white;");
    ui->pushButton_Aeroport9->setStyleSheet("background-color: white;");

    ui->pushButton_AeroportdeToulon->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeBastia->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeMarseille->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeToulouse->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdePau->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeLyon->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeMontpellier->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeNice->setStyleSheet("background-color: white;");
    ui->pushButton_AeroportdeBiarritz->setStyleSheet("background-color: white;");

};


void Dialog::on_pushButton_Aeroport1_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick1+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick1%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_1->hide();
        ui->pushButton_AeroportdeToulon->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport1->setStyleSheet("background-color: white;");

    }
    else{

        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_AeroportdeToulon->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné
        ui->pushButton_Aeroport1->setStyleSheet("background-color: red;");

        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_1->show();

        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.0962276, 6.1498611");
        ui->listWidget_Complete->addItem("Name : Aéroport de Toulon-Hyères");
        ui->listWidget_Complete->addItem("IATA code : TLN");
        ui->listWidget_Complete->addItem("ICAO code : LFTH");
        ui->listWidget_Complete->addItem("Wikidata id : Q543189");
        ui->listWidget_Complete->addItem("Operator : CCI du Var");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 50");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };

}

void Dialog::on_pushButton_Aeroport2_clicked()
{
    cptAiportClick1=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick2+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick2%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_2->hide();
        ui->pushButton_AeroportdeBastia->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport2->setStyleSheet("background-color: white;");

    }
    else{

        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_Aeroport2->setStyleSheet("background-color: red;");
        ui->pushButton_AeroportdeBastia->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné

        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_2->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 42.5546214, 9.4822981");
        ui->listWidget_Complete->addItem("Name : Aéroport international de Bastia-Poretta");
        ui->listWidget_Complete->addItem("Name(en) : International airport of Bastia-Poretta");
        ui->listWidget_Complete->addItem("IATA code : BIA");
        ui->listWidget_Complete->addItem("ICAO code : LFKB");
        ui->listWidget_Complete->addItem("Wikidata id : Q1430951");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 20");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };
}

void Dialog::on_pushButton_Aeroport3_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick1=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick3+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick3%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_3->hide();
        ui->pushButton_AeroportdeMarseille->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport3->setStyleSheet("background-color: white;");

    }
    else{

        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_Aeroport3->setStyleSheet("background-color: red;");
        ui->pushButton_AeroportdeMarseille->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné

        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_3->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.4375586, 5.2135912");
        ui->listWidget_Complete->addItem("Name : Aéroport de Marseille Provence");
        ui->listWidget_Complete->addItem("IATA code : MRS");
        ui->listWidget_Complete->addItem("ICAO code : LFML");
        ui->listWidget_Complete->addItem("Wikidata id : Q651190");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 10");
        ui->listWidget_Complete->addItem("");


        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };

}


void Dialog::on_pushButton_Aeroport4_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick1=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick4+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick4%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_4->hide();
        ui->pushButton_AeroportdeToulouse->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport4->setStyleSheet("background-color: white;");
    }
    else{

        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_Aeroport4->setStyleSheet("background-color: red;");
        ui->pushButton_AeroportdeToulouse->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné


        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_4->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.6299473, 1.3637499");
        ui->listWidget_Complete->addItem("Name : Aéroport de Toulouse-Blagnac");
        ui->listWidget_Complete->addItem("IATA code : TLS");
        ui->listWidget_Complete->addItem("ICAO code : LFBO");
        ui->listWidget_Complete->addItem("Wikidata id : Q372615");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 300");
        ui->listWidget_Complete->addItem("");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };

}

void Dialog::on_pushButton_Aeroport5_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick1=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick5+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick5%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_5->hide();
        ui->pushButton_AeroportdePau->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport5->setStyleSheet("background-color: white;");

    }
    else{
        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_AeroportdePau->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné
        ui->pushButton_Aeroport5->setStyleSheet("background-color: red;");

        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_5->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.3791392, -0.4201974");
        ui->listWidget_Complete->addItem("Name : Aéroport de Pau Pyrénées");
        ui->listWidget_Complete->addItem("Name(en) : Pau Pyrenees Airport");
        ui->listWidget_Complete->addItem("IATA code : PUF");
        ui->listWidget_Complete->addItem("ICAO code : LFBP");
        ui->listWidget_Complete->addItem("Operator : Pau Chamber of Commerce");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 1000");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };

}

void Dialog::on_pushButton_Aeroport6_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick1=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick6+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick6%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_6->hide();
        ui->pushButton_AeroportdeLyon->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport6->setStyleSheet("background-color: white;");

    }
    else{

        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_AeroportdeLyon->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné
        ui->pushButton_Aeroport6->setStyleSheet("background-color: red;");


        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_6->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 45.7245281, 5.0873268");
        ui->listWidget_Complete->addItem("Name : Aéroport de Lyon Saint-Exupéry");
        ui->listWidget_Complete->addItem("Name(en) : Lyon Saint-Exupéry Airport");
        ui->listWidget_Complete->addItem("IATA code : LYS");
        ui->listWidget_Complete->addItem("ICAO code : LFLL");
        ui->listWidget_Complete->addItem("Wikidata id : Q1547");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 800");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };
}


void Dialog::on_pushButton_Aeroport7_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick1=0;
    cptAiportClick8=0;
    cptAiportClick9=0;

    cptAiportClick7+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick7%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_7->hide();
        ui->pushButton_AeroportdeMontpellier->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport7->setStyleSheet("background-color: white;");

    }
    else{

        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_AeroportdeMontpellier->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné
        ui->pushButton_Aeroport7->setStyleSheet("background-color: red;");


        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_7->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.5798377, 3.9664417");
        ui->listWidget_Complete->addItem("Aéroport de Montpellier - Méditerranée");
        ui->listWidget_Complete->addItem("IATA code : MPL");
        ui->listWidget_Complete->addItem("ICAO code : LFMT");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 40");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");


        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };

}


void Dialog::on_pushButton_Aeroport8_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick1=0;
    cptAiportClick9=0;

    cptAiportClick8+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick8%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_8->hide();
        ui->pushButton_AeroportdeNice->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport8->setStyleSheet("background-color: white;");

    }
    else{
        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_AeroportdeNice->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné
        ui->pushButton_Aeroport8->setStyleSheet("background-color: red;");


        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_8->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.6597926, 7.2150735");
        ui->listWidget_Complete->addItem("Name : Aéroport de Nice-Côte d'Azur");
        ui->listWidget_Complete->addItem("Name(en) : Nice Côte d'Azur Airport");
        ui->listWidget_Complete->addItem("IATA code : NCE");
        ui->listWidget_Complete->addItem("ICAO code : LFMN");
        ui->listWidget_Complete->addItem("Wikidata id : Q821557");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 20");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };
}


void Dialog::on_pushButton_Aeroport9_clicked()
{
    cptAiportClick2=0;//Réinitilisation des autres clicks
    cptAiportClick3=0;
    cptAiportClick4=0;
    cptAiportClick5=0;
    cptAiportClick6=0;
    cptAiportClick7=0;
    cptAiportClick8=0;
    cptAiportClick1=0;

    cptAiportClick9+=1;//Compteur de click pour savoir où on en est. (Clique 1 fois=affichage, une 2ème fois=état initial)

    if(cptAiportClick9%2==0){ //Si on click 2 fois d'affilé, on revient à l'état initial
        ui->listWidget_Complete->hide();
        ui->label_Complete->hide();
        ui->label_Coordonnees_9->hide();
        ui->pushButton_AeroportdeBiarritz->setStyleSheet("background-color: white;");
        ui->pushButton_Aeroport9->setStyleSheet("background-color: white;");
    }
    else{
        InitStateAirportButton();// Remet la liste et les boutons à l'état initial

        ui->pushButton_AeroportdeBiarritz->setStyleSheet("background-color: red;");//Coloration de l'élément séléctionné
        ui->pushButton_Aeroport9->setStyleSheet("background-color: red;");

        ui->listWidget_Complete->show();
        ui->label_Complete->show();
        ui->label_Coordonnees_9->show();


        ui->listWidget_Complete->clear();
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("Geo point : 43.4691926, -1.5231207");
        ui->listWidget_Complete->addItem("Name : Aéroport de Biarritz Pays Basque");
        ui->listWidget_Complete->addItem("IATA code : BIQ");
        ui->listWidget_Complete->addItem("ICAO code : LFBZ");
        ui->listWidget_Complete->addItem("Country : France");
        ui->listWidget_Complete->addItem("Country code : FR");
        ui->listWidget_Complete->addItem("Altitude : 10");
        ui->listWidget_Complete->addItem("");
        ui->listWidget_Complete->addItem("");

        AlignItemsListAirports();//Aligne les items de la fiche Airport
    };
}

//Les mêmes fonctions mais en cliquant sur la liste

void Dialog::on_pushButton_AeroportdeToulon_clicked()
{
    on_pushButton_Aeroport1_clicked();

}


void Dialog::on_pushButton_AeroportdeBastia_clicked()
{
    on_pushButton_Aeroport2_clicked();
}


void Dialog::on_pushButton_AeroportdeMarseille_clicked()
{
    on_pushButton_Aeroport3_clicked();

}


void Dialog::on_pushButton_AeroportdeToulouse_clicked()
{
    on_pushButton_Aeroport4_clicked();

}


void Dialog::on_pushButton_AeroportdePau_clicked()
{
    on_pushButton_Aeroport5_clicked();

}


void Dialog::on_pushButton_AeroportdeLyon_clicked()
{
    on_pushButton_Aeroport6_clicked();


}


void Dialog::on_pushButton_AeroportdeMontpellier_clicked()
{
    on_pushButton_Aeroport7_clicked();

}


void Dialog::on_pushButton_AeroportdeNice_clicked()
{
    on_pushButton_Aeroport8_clicked();
}


void Dialog::on_pushButton_AeroportdeBiarritz_clicked()
{
    on_pushButton_Aeroport9_clicked();
}


void Dialog::on_pushButton_clicked()
{
    ui->flightView->setName(flightList.first()->getName());

}

