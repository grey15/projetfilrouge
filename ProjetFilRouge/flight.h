#ifndef FLIGHT_H
#define FLIGHT_H

#include <QWidget>
#include <QMap>
#include <QJsonArray>
#include <QListWidget>
#include <QDialog>

#include "posilistitem.h"
#include "flightdata.h"

namespace Ui {
class Flight;
}

class Flight : public QDialog
{
    Q_OBJECT

public:

    Flight(QWidget *parent = nullptr);
    ~Flight();


    void setName(QString _name);
    QString getName();
    QVector<FlightData> getPosiHist();
    void addPosiHist(FlightData);
    void fillPosiHist(QVector<FlightData>);

private:
    Ui::Flight *ui;
    QString name;
    QVector<FlightData> posiHist;


};
#endif // FLIGHT_H
