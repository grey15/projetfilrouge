#ifndef POSILISTITEM_H
#define POSILISTITEM_H

#include <QWidget>

namespace Ui {
class PosiListItem;
}

class PosiListItem : public QWidget
{
    Q_OBJECT

public:
    PosiListItem(QWidget *parent = nullptr);
    ~PosiListItem();
    void setHeure(int);
    void setMinute(int);
    void setLongitude(double);
    void setLatitude(double);
    void setAltitude(double);

private:
    Ui::PosiListItem *ui;
    int heure;
    int minute;
    double longitude;
    double latitude;
    double altitude;


};

#endif // POSILISTITEM_H
