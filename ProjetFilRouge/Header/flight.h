#ifndef FLIGHT_H
#define FLIGHT_H

#include <QWidget>
#include <QMap>
#include <QJsonArray>
#include <QListWidget>

#include "Header/posilistitem.h"
#include "Header/flightdata.h"

namespace Ui {
class Flight;
}

class Flight : public QWidget
{
    Q_OBJECT

public:

    Flight(QWidget *parent = nullptr);
    ~Flight();

    void display();

    void setName(QString _name);
    QString getName();
    QVector<FlightData> getPosiHist();
    void addPosiHist(FlightData _posiHist);

private:
    Ui::Flight *ui;
    QString name;
    QVector<FlightData> posiHist;


};
#endif // FLIGHT_H
