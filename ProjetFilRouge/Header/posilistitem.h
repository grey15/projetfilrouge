#ifndef POSILISTITEM_H
#define POSILISTITEM_H

#include <QWidget>

namespace Ui {
class PosiListItem;
}

class PosiListItem : public QWidget
{
    Q_OBJECT

public:
    PosiListItem(QWidget *parent = nullptr);
    ~PosiListItem();

private:
    Ui::PosiListItem *ui;
};

#endif // POSILISTITEM_H
