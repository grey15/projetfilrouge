#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QVector>
#include <QFile>

#include "Header/flight.h"
#include "Header/flightdata.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void AlignItemsListAirports();

    void InitStateAirportButton();

    void on_pushButton_Aeroport1_clicked();

    void on_pushButton_Aeroport2_clicked();

    void on_pushButton_Aeroport3_clicked();

    void on_pushButton_Aeroport4_clicked();

    void on_pushButton_Aeroport5_clicked();

    void on_pushButton_Aeroport6_clicked();

    void on_pushButton_Aeroport7_clicked();

    void on_pushButton_Aeroport8_clicked();

    void on_pushButton_Aeroport9_clicked();

    void on_pushButton_AeroportdeToulon_clicked();

    void on_pushButton_AeroportdeBastia_clicked();

    void on_pushButton_AeroportdeMarseille_clicked();

    void on_pushButton_AeroportdeToulouse_clicked();

    void on_pushButton_AeroportdePau_clicked();

    void on_pushButton_AeroportdeLyon_clicked();

    void on_pushButton_AeroportdeMontpellier_clicked();

    void on_pushButton_AeroportdeNice_clicked();

    void on_pushButton_AeroportdeBiarritz_clicked();

    void on_pushButton_clicked();

private:
    Ui::Dialog *ui;
    int cptAiportClick1=0;
    int cptAiportClick2=0;
    int cptAiportClick3=0;
    int cptAiportClick4=0;
    int cptAiportClick5=0;
    int cptAiportClick6=0;
    int cptAiportClick7=0;
    int cptAiportClick8=0;
    int cptAiportClick9=0;
//    Flight *flight1;
    QVector<Flight*> flightList;
    FlightData flightData;
};

#endif // DIALOG_H
