#ifndef REGISTER_H
#define REGISTER_H

#include <QDialog>
#include <QMainWindow>
#include <QMessageBox>

namespace Ui {
class Register;
}

class Register : public QDialog
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = nullptr);
    ~Register();



private slots:
    void on_pushButton_Create_new_account_clicked();

private:
    Ui::Register *ui;
};

#endif // REGISTER_H
