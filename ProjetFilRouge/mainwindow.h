#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "register.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_Quitter_clicked();

    void on_pushButton_Log_in_clicked();

    void on_pushButton_Create_an_account_clicked();


private:
    Ui::MainWindow *ui;
    int cpt=0;
};
#endif // MAINWINDOW_H
