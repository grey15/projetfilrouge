#include "flightdata.h"

FlightData::FlightData()
{

}

FlightData::~FlightData()
{
}


// les setter de la class

void FlightData::setHeure(int _heure){
    this->heure = _heure;
}
void FlightData::setMinute(int _minute){
    this->minute = _minute;
}
void FlightData::setLongitude(double _longitude){
    this->longitude = _longitude;
}
void FlightData::setLatitude(double _latitude){
    this->latitude = _latitude;
}
void FlightData::setAltitude(double _altitude){
    this->altitude = _altitude;
}



// les getter de la class

int FlightData::getHeure(){
    return this->heure;
}
int FlightData::getMinute(){
    return this->minute;
}
double FlightData::getLongitude(){
    return this->longitude;
}
double FlightData::getLatitude(){
    return this->latitude;
}
double FlightData::getAltitude(){
    return this->altitude;
}
