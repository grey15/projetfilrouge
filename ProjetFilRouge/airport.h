#ifndef AIRPORT_H
#define AIRPORT_H

#include <QDialog>

namespace Ui {
class Airport;
}

class Airport : public QDialog
{
    Q_OBJECT

public:
    explicit Airport(QWidget *parent = nullptr);
    ~Airport();

    void setLongitude(double);
    void setLatitude(double);
    void setAltitude(double);
    void setName(QString);
    void setNameEn(QString);
    void setNameFr(QString);
    void setIATACode(QString);
    void setICAOCode(QString);
    void setWikidataId(QString);
    void setOperator(QString);
    void setCountry(QString);
    void setCountryCode(QString);

    double getLongitude();
    double getLatitude();
    double getAltitude();
    QString getName();
    QString getNameEn();
    QString getNameFr();
    QString getIATACode();
    QString getICAOCode();
    QString getWikidataId();
    QString getOperator();
    QString getCountry();
    QString getCountryCode();


private:
    Ui::Airport *ui;
    double longitude;
    double latitude;
    double altitude;
    QString name;
    QString nameEn;
    QString nameFr;
    QString IATACode;
    QString ICAOCode;
    QString wikidataId;
    QString sOperator;
    QString country;
    QString countryCode;
};

#endif // AIRPORT_H
